# Hangman

Game played by single or double players, this improves our vocabulary and topical knowledge. The player will be guessing the letters of the random word. If the word is guessed correctly then the player wins, if not the man will be hanged.

**Rules to play the Game**
- The chooser draws a number of dashes equivalent to the number of letters in the word.
- If a guessed letter appears in the word, all instances of it are revealed. If not guesser loses a chance.
- The guesser chances are tracked using a stick figure drawing of person being hanged from a gallows.
- The figure is drawn one body part at a time.
- The guesser loses when the entire figure has been drawn

**Team Members**
- V.Gnapana : 19WH1A05H1 : CSE
- G.Mamatha : 19WH1A0455 : ECE
- V.Anusha : 19WH1A05E5 : CSE
- G.Lakshmi Priya :19WH1A1264 : IT 
- M.Sreeja : 19WH1A0588 : CSE 
- K.Shivani : 19WH1A0225 : EEE
